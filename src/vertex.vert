attribute vec4 aVertexPosition;
attribute vec4 aVertexColor;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;

varying lowp vec4 vColor;
//varying vec4 v_color;
void main() {
    gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
//    gl_Position = aVertexPosition;
//    vColor = gl_Position * 0.1 + 0.1;
    vColor = aVertexColor;
}
