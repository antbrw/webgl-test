import vert from "./vertex.vert";
import frag from "./fragment.frag";

import drawScene from "./drawScene";
import initBuffers from "./initBuffers";
import initShaderProgram from "./initShaderProgram";
import setColor from "./setColor";

export default function (canvas) {
    const gl = canvas.getContext("webgl");

    gl.canvas.width = canvas.clientWidth
    gl.canvas.height = canvas.clientHeight

    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)

    gl.clear(gl.COLOR_BUFFER_BIT);

    const shaderProgram = initShaderProgram(gl, vert, frag);

    const programInfo = {
        program: shaderProgram,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram, "aVertexPosition"),
            vertexColor: gl.getAttribLocation(shaderProgram, 'aVertexColor')
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(
                shaderProgram,
                "uProjectionMatrix"
            ),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, "uModelViewMatrix")
        }
    };


    const timeLoc = gl.getUniformLocation(shaderProgram, 'time')
    let squareRotation = 0.0
    let then = 0
    const buffers = initBuffers(gl)

    function loop(timeStamp) {
        const now = timeStamp * 0.001
        const deltaTime = now - then
        then = now
        setColor(gl, programInfo, buffers)
        drawScene(gl, programInfo, buffers, deltaTime, squareRotation);

        // color over time
        gl.uniform1f(timeLoc, now)
        window.requestAnimationFrame(loop)
        squareRotation += deltaTime
    }

    window.requestAnimationFrame(loop)

}
