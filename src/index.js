import "./styles.css";
import main from "./main";

const canvas = document.getElementById("canvas");
canvas.style.height = '100vh'
canvas.style.width = '100vw'
main(canvas);
