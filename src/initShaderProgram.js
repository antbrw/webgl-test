import loadShader from "./loadShader";
export default function initShaderProgram(gl, vsSource, fsSource) {
  const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);
  const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);

  // Create the shader program

  const shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);

  // If creating the shader program failed, alert

  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert(
      "Unable to initialize the shader program: " +
        gl.getProgramInfoLog(shaderProgram)
    );
    return null;
  }

  return shaderProgram;
}
